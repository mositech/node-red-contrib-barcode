module.exports = function(RED) {
  'use strict';
  const parser = require('./parser');
  let hid = require('node-hid');

  function BarcodeScannerNode(config) {
    RED.nodes.createNode(this, config);

    this.topic = config.topic;
    this.vendorId = config.vid;
    this.productId = config.pid;

    let node = this;
    let path = null;
    let tout;
    let input = '';

    let getBarcodeStr = function (barcode) {
      const parsed = parser(barcode);
      if (parsed.empty()) {
        // TODO
      } else {
        const newChar = parsed.charCodes[0];
        input += newChar;
        if (newChar === '\n') {
          const result = input;
          input = '';
          return result;
        }
        return '';
      }
    };

    let findScanner = function () {
      node.log('Looking for scanner...');
      let devices = hid.devices();
      for (let dev = 0; dev < devices.length; dev++) {
        if ((devices[dev].vendorId == node.vendorId) && (devices[dev].productId == node.productId)) {
          path = devices[dev].path;
          node.log('Found scanner at: '+path);
          break;
        }
      }
      if (path === null) {
        tout = setTimeout( function () {
          findScanner();
        }, 15000);
      }
    };
    findScanner();

    if (path != null) {
      try {
        node.scanner = new hid.HID(path);

        this.status({
          fill: 'green',
          shape: 'dot',
          text: 'connected'
        });

        node.scanner.on('data', function(code) {
          let barcodeStr = getBarcodeStr(code);
          if (barcodeStr !== '' && barcodeStr !== undefined) {
            let msg = {
              topic: node.topic,
              payload: barcodeStr.trim()
            };
            node.send(msg);
          }
        });
      } catch (err) {
        this.status({
          fill: 'red',
          shape: 'ring',
          text: 'disconnected'
        });
        console.log(err);
        node.warn('Cannot open USB scanner, do you have permission to access?');
      }
    } else {
      findScanner();
    }

    this.on('close', function() {
      node.log('Releasing scanner...');
      if (tout) { clearTimeout(tout); }
      if (node.scanner) { node.scanner.close(); }
    });
  }

  RED.nodes.registerType('barcode-scanner', BarcodeScannerNode);
};