# node-red-contrib-barcode #
An USB barcode scanner node for Node-RED.

## Installation Notes for Linux (Raspbian) ##
Copy the file *39-barcode.rules* to ```/etc/udev/rules.d/```, open the file with an editor will show:
```
# CHANGE idVendor and idProduct to the actual vid, pid of the ATTACHED barcode scanner
SUBSYSTEM=="usb", ATTR{idVendor}=="1234", ATTR{idProduct}=="4321", MODE="0666", GROUP="plugdev"
```
For the rules to work *idVendor* and *idProduct* need to be replaced with the values of the USB scanner that is attached to the system. The values can be found with the ```lsusb``` command.

After editing the rules file, add users who need access to the USB device to the *plugdev* group, then reload rules configurations, i.e.,
```
$ sudo gpasswd USERNAME plugdev
$ sudo udevadm control --reload-rules
```
or reboot the system for the permission setting to take effect.